/* generated by bin2h, do not edit */

#define sizeof_boot_bin 512
extern unsigned char binary_boot_bin[512];
#define sizeof_bootboot_bin 13312
extern unsigned char binary_bootboot_bin[9329];
#define sizeof_bootboot_efi 103102
extern unsigned char binary_bootboot_efi[46246];
#define sizeof_bootboot_img 35344
extern unsigned char binary_bootboot_img[20050];
#define sizeof_bootboot_rv64 8192
extern unsigned char binary_bootboot_rv64[31];
#define sizeof_LICENCE_broadcom 1594
extern unsigned char binary_LICENCE_broadcom[883];
#define sizeof_bootcode_bin 52476
extern unsigned char binary_bootcode_bin[30922];
#define sizeof_fixup_dat 7269
extern unsigned char binary_fixup_dat[1270];
#define sizeof_start_elf 2979264
extern unsigned char binary_start_elf[1740943];
